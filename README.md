# Project to show/test WFS capabilities of QGIS in combination with GeoGirafe

## Main setup

We use the standard GeoGirafe application from [DockerHub](https://hub.docker.com/r/geogirafe/viewer).
All config is setup in [this folder](./deploy-config/webgis/). Corresponding to the
[QGIS Project](./deploy-config/wfs/projects/test/test.qgs) we offer one polygon layer as WFS. This is
configured in the [themes.json](./deploy-config/webgis/themes.json).

###

- [WebGIS](http://localhost:8081)
- [QGIS-Server](http://localhost:8088)

The current setup shows a working example of QGIS-WFS with GeoGirafe.

## Known problems

Already while setting up, some quirks came to attention.

### Layername of published layer

There is a not clear correlation between layername in QGIS layertree, layer QGIS-Server settings AND published WFS layer in QGIS Project settings.

The situation was as follows:

layername in layertree: 'Kantonsgrenzen'
shortname in layer settings (QGIS-Server): 'kantonsgrenzen'
title in layer settings (QGIS-Server): 'Kantonsgrenzen'
layername in project settings QGIS-Server WFS tab: 'Kantonsgrenzen'

This lead to an error where no typeName="feature:kantonsgrenzen" was found. Only after renaming QGIS layer in layertree
to 'kantonsgrenzen' lead to success. So there is some stange coincedence.


# QGIS Server endpoints

This setup delivers a central project page produced by nginx:

=> [Projects](http://localhost:8088/projects/)


# WFS Endpoint testing

## GetCapabilities

```shell
curl -X GET "http://test.localhost:8088/ogc/?SERVICE=WFS&REQUEST=GetCapabilities&VERSION=1.1.0"
```

## Query with a BBOX

**Note:** The following request uses the content of [this file](request_bodies/bbox.xml)

```shell
curl -X POST -d @request_bodies/bbox.xml "http://test.localhost:8088/ogc/" > answer.xml
```

## Query with a Parameter

**Note:** The following request uses the content of [this file](request_bodies/parameter.xml)

```shell
curl -X POST -d @request_bodies/parameter.xml "http://test.localhost:8088/ogc/" > answer.xml
```
